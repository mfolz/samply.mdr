# Changelog

## Version 1.8.0 (unreleased)

- added the maven site documentation
- added a guide on how to install an MDR on a new server
